const { Schema, model } = require('mongoose')

const ActSchema = new Schema({
    nombre: { type: String, required: true },
    folio: { type: String, required: true },
    periodo: { type: String, required: true },
    status: { type: Boolean, default: false }
}, {
    timestamps: true
})

module.exports = model('Act', ActSchema)
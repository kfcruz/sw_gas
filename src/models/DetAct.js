const { Schema, model } = require('mongoose')

const DetActSchema = new Schema({
    act: { type: String, required: true },
    fecha: { type: String, default: Date.now(), required: true },
    horain: { type: String, required: true },
    horate: { type: String, required: true },
    tipo: { type: String, required: true },
    area: { type: String, required: true },
    descripcion: { type: String, required: true },
    autoriza: { type: String, required: true },
    realiza: { type: String },
    imgs: { type: [String], required: false },
    mat: {
        her: { type: String },
        eq: { type: String },
        sq: { type: String }
    },
    contratista: {
        nombre: { type: String },
        compania: { type: String }
    }
}, {
    timestamps: true
})

module.exports = model('DetAct', DetActSchema)
const express = require('express')
const exphbs = require('express-handlebars')
const path = require('path')
const methodOverride = require('method-override')
const session = require('express-session')
const flash = require('connect-flash')
const morgan = require('morgan')
const passport = require('passport')
const multer = require('multer')
const uuid = require('uuid/v4')
const { format } = require('timeago.js')
const favicon = require('serve-favicon')

// Initializations
const app = express()
require('./config/passport')

// Settings
app.set('port', process.env.PORT || 3000)
app.set('views', path.join(__dirname, 'views'))
app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    extname: '.hbs'
}));
app.set('view engine', '.hbs')

// Middlewares
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(express.urlencoded({ extended: false }))
app.use(methodOverride('_method'))
app.use(morgan('dev'))
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(flash())

const storage = multer.diskStorage({
    destination: path.join(__dirname, 'public/img/uploads/'),
    filename: (req, file, cb, filename) => {
        cb(null, uuid() + path.extname(file.originalname).toLocaleLowerCase())
    }
})
app.use(multer({
    storage,
    dest: path.join(__dirname, 'public/img/uploads/'),
    limits: { fieldSize: 3145728 },
    fileFilter: (req, file, cb) => {
        const filetypes = /jpeg|jpg|png|pdf/;
        const mimetype = filetypes.test(file.originalname)
        const extname = filetypes.test(path.extname(file.originalname))
        if (mimetype && extname) {
            return cb(null, true)
        }
        cb('Error debe ser una archivo válido jpeg|jpg|png|pdf')
    }
}).array('images', 6))

// Global Variables
app.use((req, res, next) => {
    res.locals.success_msg = req.flash('success_msg')
    res.locals.error_msg = req.flash('error_msg')
    res.locals.error = req.flash('error')
    res.locals.user = req.user || null
    
    app.locals.format = format

    next()
})

// Routes
app.use(require('./routes/index.routes'))
app.use(require('./routes/users.routes'))
app.use(require('./routes/activities.routes'))

// Static Files
app.use(express.static(path.join(__dirname, 'public')))

module.exports = app
const indexCtrl = {}
const Act = require('../models/Act')
const DetAct = require('../models/DetAct')

indexCtrl.renderIndex = (req, res) => {
    res.render('index')
}

indexCtrl.renderAbout = (req, res) => {
    res.render('about')
}

indexCtrl.renderHome = async (req, res) => {
    const actividades = await Act.find({ status: false })
    const contador = actividades.length
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth()
    const day = date.getDate()
    let query = ""

    if ((month + 1) > 10) {
        if (day > 10) {
            query = year + '-' + (month + 1) + '-' + day;
        } else {
            query = year + '-' + (month + 1) + '-' + '0' + day;
        }
    } else {
        if (day > 10) {
            query = year + '-' + '0' + (month + 1) + '-' + day;
        } else {
            query = year + '-' + '0' + (month + 1) + '-' + '0' + day;
        }
    }
    let detalle = await DetAct.findOne({ fecha: query })
    if (detalle != null) {
        detalle = false
    } else {
        detalle = true
    }
    try {
        for (let i = 0; i < actividades.length; i++) {
            const idAct = actividades[i]._id
            const detAct = await DetAct.find({ act: idAct })
            const fecha = detAct[0].fecha
            actividades[i].fecha = fecha
        }
    } catch (error) {
        req.flash('error_msg', 'No se puede iterar sobre la colección' + error)
    }
    res.render('home', { actividades, contador, detalle })
}

module.exports = indexCtrl
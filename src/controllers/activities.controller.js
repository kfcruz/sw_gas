const activitiesCtrl = {}
const Act = require('../models/Act')
const DetAct = require('../models/DetAct')
const User = require('../models/User')
const Image = require('../models/Image')
const path = require('path')

activitiesCtrl.renderActivityForm = async (req, res) => {
    const admin = await User.find({ admin: true })
    const us = await User.find({ admin: false })
    res.render('activities/create-activity', { admin, us })
}

activitiesCtrl.createNewActivity = async (req, res) => {
    //Se guarda la actividad
    const { nombre, periodo, folio, fecha, hi, ht, tipom, aread, descripcion,
        aut, ej, her, eq, sq, contra, compania } = req.body
    const newAct = new Act({ nombre, folio, periodo })
    await newAct.save()
    //Se consulta el último id agregado de la actividad
    const idAc = await Act.findOne({}, { "_id": 1 }).sort({ createdAt: 'desc' })
    const id = idAc._id
    //Se guardan las imágenes
    const imgs = []
    for (const i in req.files) {
        const image = new Image()
        image.filename = req.files[i].filename
        image.path = '/img/uploads/' + req.files[i].filename
        image.originalname = req.files[i].originalname
        image.mimetype = req.files[i].mimetype
        image.size = req.files[i].size

        await image.save()

        //Consultamos las ultimas imagenes insertadas
        const idImg = await Image.findOne({}, { "_id": 1 }).sort({ createdAt: 'desc' })
        imgs.push(idImg._id)
    }
    //Validación si existe un contratista
    try {
        if (contra && compania) {
            let newDetAct = new DetAct({
                act: id, fecha, horain: hi, horate: ht, tipo: tipom, area: aread, descripcion,
                autoriza: aut, realiza: ej, imgs, mat: { her, eq, sq }, contratista: { nombre: contra, compania }
            })
            //Se Guarda el detalle de la actvidad con sus campos
            await newDetAct.save()
        } else {
            const newDetAct = new DetAct({
                act: id, fecha, horain: hi, horate: ht, tipo: tipom, area: aread, descripcion,
                autoriza: aut, realiza: ej, imgs, mat: { her, eq, sq }
            })
            //Se Guarda el detalle de la actvidad con sus campos
            await newDetAct.save()
        }
        req.flash('success_msg', 'Actividad agregada correctamente')
        res.redirect('/home')
    } catch (error) {
        res.redirect('/home')
        req.flash('error_msg', 'No se puede registrar la actividad' + error)
    }
}

activitiesCtrl.listAllActivity = async (req, res) => {
    const actividad = await Act.find()

    try {
        for (const i in actividad) {
            const idAct = actividad[i]._id
            const detAct = await DetAct.findOne({ act: idAct })
            const fecha = detAct.fecha
            actividad[i].fecha = fecha
        }
    } catch (error) {
        console.log('error_msg', 'No se puede iterar sobre la colección' + error)
    }

    res.render('activities/view-activities', { actividad })
}

activitiesCtrl.listDetailActivity = async (req, res) => {
    const { idc } = req.body
    const detalleActividad = []

    if (!Array.isArray(idc)) {
        const detalle = await DetAct.findOne({ act: idc })
        detalleActividad.push(detalle)
    } else {
        for (const i in idc) {
            const detalle = await DetAct.findOne({ act: idc[i] })
            detalleActividad.push(detalle)
        }
    }

    res.render('activities/listDetailActivity', { detalleActividad })
}

activitiesCtrl.renderEditForm = async (req, res) => {
    const detAct = await DetAct.findOne({ act: req.params.id })
    const act = await Act.findById(req.params.id)

    const cant = detAct.imgs.length

    const ids = detAct.imgs;
    const imagesPath = [];

    for (const i in ids) {
        const image = await Image.findOne({ _id: ids[i] }, { "path": 1 })
        imagesPath.push(image.path)
    }

    res.render('activities/edit-activity', { detAct, act, cant, imagesPath })
}

activitiesCtrl.updateActivity = async (req, res) => {
    const { descripcion } = req.body
    await DetAct.findByIdAndUpdate(req.params.id, { descripcion })
    const detalleActividad = await DetAct.find().sort({ updatedAt: 'desc' })
    req.flash('success_msg', 'Actividad editada correctamente')
    res.render('activities/listDetailActivity', { detalleActividad })
}

activitiesCtrl.renderFormReport = async (req, res) => {
    let { idD } = req.body
    if (idD == null) {
        idD = req.params.id
    }
    const detalleActividad = []
    let ids = []
    let contador = 0;

    try {
        if (!Array.isArray(idD)) {
            const detalle = await DetAct.findOne({ _id: idD })
            const idA = detalle.act

            const actividad = await Act.findOne({ _id: idA })
            detalle.nombre = actividad.nombre
            detalle.folio = actividad.folio
            detalle.periodo = actividad.periodo

            ids = detalle.imgs;
            contador = detalle.imgs.length
            detalle.imgs = []
            detalle.cont = contador

            for (const i in ids) {
                const image = await Image.findOne({ _id: ids[i] }, { "path": 1 })
                detalle.imgs.push(image.path)
            }
            detalleActividad.push(detalle)
        } else {
            for (const i in idD) {
                const detalle = await DetAct.findOne({ _id: idD[i] })
                const idA = detalle.act

                const actividad = await Act.findOne({ _id: idA })
                detalle.nombre = actividad.nombre
                detalle.folio = actividad.folio
                detalle.periodo = actividad.periodo

                ids = detalle.imgs;
                contador = detalle.imgs.length
                detalle.imgs = []
                detalle.cont = contador

                for (const i in ids) {
                    const image = await Image.findOne({ _id: ids[i] }, { "path": 1 })
                    detalle.imgs.push(image.path)
                }

                detalleActividad.push(detalle)
            }
        }
    } catch (error) {
        req.flash('error_msg', 'No se puede iterar sobre la colección ' + error)
    }

    res.render('activities/report', { detalleActividad })
}

activitiesCtrl.CalendarForm = async (req, res) => {
    let fecha = new Date()
    let queryDays = []
    //let { month, year } = req.body
    month = fecha.getMonth()
    year = fecha.getFullYear()

    /*if (month) {
        month = fecha.getMonth()
    }
    if (year) {
        year = fecha.getFullYear()
    }*/
    
    let dayM = new Date(year, month + 1, 0).getDate()
    let query = ""

    for (let i = 1; i <= dayM; i++) {
        if ((month + 1) >= 10) {
            if (i > 10) {
                query = year + '-' + (month + 1) + '-' + i;
            } else {
                query = year + '-' + (month + 1) + '-' + '0' + i;
            }
        } else {
            if (i >= 10) {
                query = year + '-' + '0' + (month + 1) + '-' + i;
            } else {
                query = year + '-' + '0' + (month + 1) + '-' + '0' + i;
            }
        }
        const detalle = await DetAct.findOne({ fecha: query }, { "_id": 1 })
        if (detalle != null) {
            queryDays.push(detalle._id);
        } else {
            queryDays.push('empty')
        }
    }

    res.render('activities/calendar', { queryDays, year, month })
}

activitiesCtrl.renderAuth = async (req, res) => {
    if (req.user.admin == false) {
        req.flash('error_msg', 'No autorizado')
        return res.redirect('/home')
    }

    const actividades = await Act.find({ status: false })

    try {
        for (const i in actividades) {
            const idAct = actividades[i]._id
            const detAct = await DetAct.findOne({ act: idAct })
            const fecha = detAct.fecha
            actividades[i].fecha = fecha
        }
    } catch (error) {
        req.flash('error_msg', 'No se puede iterar sobre la colección' + error)
    }

    res.render('activities/auth-activities', { actividades })
}

activitiesCtrl.updateAuth = async (req, res) => {
    let { aut } = req.body

    if (!Array.isArray(aut)) {
        await Act.findByIdAndUpdate(aut, { status: true })
    } else {
        for (const i in aut) {
            await Act.findByIdAndUpdate(aut[i], { status: true })
        }
    }
    res.render('activities/auth-activities')
}

activitiesCtrl.CalendarFormID = async (req, res) => {
    /*let fecha = new Date()
    let calendario = [7][6]
    let day = 0
    let month = req.body
    let year = req.body
    if (month == null) {
        month = fecha.getMonth()
    }
    if (year == null) {
        year = fecha.getFullYear()
    }
    let dayM = new Date(year, month, 0)
    let firstMonth = new Date(year, month - 1, 1);
    let firstDay = (firstMonth.getDay() == 0) ? 7 : firstMonth.getDay();
    let lastMonth = dayM.getDate()
    let lastCell = firstDay + lastMonth
    let queryDays = []

    for (let i = 0; i < 6; i++) {
        for (let j = 0; j < 7; j++) {
            if (j == firstDay) {
                day = 1
            }
            if ((i < firstDay) || (i > lastCell)) {
                calendario[j][i] = " "
            } else {
                if ((day == fecha.getDate()) && (month == fecha.getMonth()) && (year == fecha.getFullYear())) {
                    calendario[j][i] = "hoy " + day
                } else {
                    calendario[j][i] = day
                }
                day++
            }
        }
    }*/
    res.send("hola")
}

module.exports = activitiesCtrl
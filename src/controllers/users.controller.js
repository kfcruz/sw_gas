const usersCtrl = {}
const User = require('../models/User')
const passport = require('passport')

usersCtrl.renderSignInForm = (req, res) => {
    res.render('users/signin')
}

usersCtrl.renderSignUpForm = (req, res) => {
    res.render('users/signup')
}

usersCtrl.signin = passport.authenticate('local', {
    successRedirect: '/home', /* RUTA DE ÉXITO */
    failureRedirect: '/users/signin',
    failureFlash: true
})

/* Obtenemos del método post del formulario, los datos enviados. */
usersCtrl.signup = async (req, res) => {
    let errors = [];
    const { nombres, apellidos, email, nempleado, cargo, password, confirm_password } = req.body

    if (password != confirm_password) {
        errors.push({ text: 'Las contraseñas no coinciden.' })
    }
    if (password.length < 4) {
        errors.push({ text: 'La contraseña deberá de ser mayor a 4 caracteres.' })
    }
    if (errors.length > 0) {
        res.render('users/signup', { errors, nombres, apellidos, email, nempleado, cargo, password, confirm_password })
    } else {
        const emailUser = await User.findOne({ email: email })
        if (emailUser) {
            req.flash('error_msg', 'El correo ya ha sido registrado.')
            res.redirect('/users/signup')
        } else {
            const newUser = new User({ nombres, apellidos, email, nempleado, cargo, password })
            newUser.password = await newUser.encryptPassword(password)
            await newUser.save()
            req.flash('success_msg', 'Registro con éxito!');
            res.redirect('/users/signin')
        }
    }
}

usersCtrl.logout = (req, res) => {
    req.logout()
    req.flash('success_msg', 'Se ha cerrdado sesión')
    res.redirect('/users/signin')
}

usersCtrl.listAll = async (req, res) => {
    if (req.user.admin == false) {
        req.flash('error_msg', 'No autorizado')
        return res.redirect('/notes')
    }

    const users = await User.find().sort({ updatedAt: 'desc' })

    res.render('users/list-users', { users })
}

usersCtrl.renderEditForm = async (req, res) => {
    if (req.user.admin == false) {
        req.flash('error_msg', 'No autorizado')
        return res.redirect('/notes')
    }

    const user = await User.findById(req.params.id)

    res.render('users/edit-user', { user })
}

usersCtrl.updateUser = async (req, res) => {
    if (req.user.admin == false) {
        req.flash('error_msg', 'No autorizado')
        return res.redirect('/notes')
    }
    
    const { nempleado, cargo, email } = req.body

    await User.findByIdAndUpdate(req.params.id, { nempleado, cargo, email })

    const users = await User.find().sort({ updatedAt: 'desc' })
    req.flash('success_msg', 'Usuario editado correctamente')
    res.render('users/list-users', { users })
}

module.exports = usersCtrl
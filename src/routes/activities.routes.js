const { Router } = require('express')
const router = Router()
const { isAuthenticated } = require('../helpers/auth')

const { renderActivityForm,
    createNewActivity,
    listAllActivity,
    listDetailActivity,
    renderEditForm,
    updateActivity,
    renderFormReport,
    CalendarForm,
    CalendarFormID,
    renderAuth,
    updateAuth } = require('../controllers/activities.controller')

//Alta de actividad
router.get('/activities/add', isAuthenticated, renderActivityForm)
router.post('/activities/create', isAuthenticated, createNewActivity)

//Consultas de actividades
router.get('/activities/all', isAuthenticated, listAllActivity)
router.post('/activities/all/listDetailActivity', isAuthenticated, listDetailActivity)

//Reporte
router.post('/activities/report', isAuthenticated, renderFormReport)
router.get('/activities/report/:id',isAuthenticated, renderFormReport)

//Edición de actividades
router.get('/activities/edit/:id', isAuthenticated, renderEditForm)
router.put('/activities/edit/:id', isAuthenticated, updateActivity)

//Autorización de actividades
router.get('/activities/auth', isAuthenticated, renderAuth)
router.put('/activities/auth', isAuthenticated, updateAuth)

//Calendario
router.get('/activities/calendar', isAuthenticated, CalendarForm)
//router.get('/activities/calendar/:id', CalendarFormID)

module.exports = router;
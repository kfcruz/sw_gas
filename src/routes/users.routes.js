const { Router } = require('express')
const router = Router()
const { isAuthenticated } = require('../helpers/auth')

const { renderSignInForm,
    renderSignUpForm,
    signin,
    signup,
    logout,
    listAll,
    renderEditForm,
    updateUser } = require('../controllers/users.controller')

//Inicio de sesión
router.get('/users/signin', renderSignInForm)
router.post('/users/signin', signin)

//Registro
router.get('/users/signup', renderSignUpForm)
router.post('/users/signup', signup)

//Cierre de sesión
router.get('/users/logout', logout)

//Consulta de usuarios
router.get('/users/list', isAuthenticated, listAll)

//Edición de usuarios
router.get('/users/edit/:id', isAuthenticated, renderEditForm)
router.put('/users/edit/:id', isAuthenticated, updateUser)

module.exports = router;
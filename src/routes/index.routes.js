const { Router } = require('express')
const router = Router()
const { isAuthenticated } = require('../helpers/auth')

const { renderIndex, renderAbout, renderHome } = require('../controllers/index.controllers')

router.get('/', renderIndex)

router.get('/about', renderAbout)

router.get('/home', isAuthenticated, renderHome)

module.exports = router
const mongoose = require('mongoose')

const { GAS_APP_HOST, GAS_APP_DB } = process.env
const MONGODB_URI = `mongodb://${GAS_APP_HOST}/${GAS_APP_DB}`

mongoose.connect(MONGODB_URI, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true
}).then(db => console.log('DB online')).catch(err => console.log(err))